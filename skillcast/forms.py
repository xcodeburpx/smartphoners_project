from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from skillcast.models import User, Circle


class RegistrationForm(FlaskForm):
    name = StringField('Imię',
                           validators=[DataRequired(), Length(min=2, max=20)])
    surname = StringField('Nazwisko',
                           validators=[DataRequired(), Length(min=2, max=20)])
    username = StringField('Nickname',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Adres email',
                        validators=[DataRequired(), Email()])

    university = SelectField('Universytet',
                           validators=[DataRequired()],
                             choices=[
                                 ("AGH", "AGH"),
                                 ("PK", "PK"),
                                 ("UJ", "UJ"),
                                 ("UR", "UR"),
                                 ("None", "None"),
                             ])

    faculty = SelectField('Wydział',
                           validators=[DataRequired()],
                          choices=[("WGiG","Wydział Górnictwa i Geoinżynierii"),
                            ("WIMiIR","Wydział Inżynierii Metali i Informatyki Przemysłowej"),
                            ("WEAiB","Wydział Elektrotechniki, Automatyki, Informatyki i Inżynierii Biomedycznej"),
                            ("WEIT","Wydział Informatyki, Elektroniki i Telekomunikacji"),
                            ("WIMiR","Wydział Inżynierii Mechanicznej i Robotyki"),
                            ("WGGiOŚ","Wydział Geologii, Geofizyki i Ochrony Środowiska"),
                            ("WGGiIŚ","Wydział Geodezji Górniczej i Inżynierii Środowiska"),
                            ("WIMiC","Wydział Inżynierii Materiałowej i Ceramiki"),
                            ("WO","Wydział Odlewnictwa"),
                            ("WMN","Wydział Metali Nieżelaznych"),
                            ("WWNiG","Wydział Wiertnictwa, Nafty i Gazu"),
                            ("WZ","Wydział Zarządzania"),
                            ("WEiP","Wydział Energetyki i Paliw"),
                            ("WFiIS","Wydział Fizyki i Informatyki Stosowanej"),
                            ("WMS","Wydział Matematyki Stosowanej"),
                            ("WH","Wydział Humanistyczny"),("None", "None")]
                          )

    social = StringField('Media społecznościowe',
                           validators=[DataRequired(), Length(min=2, max=20)])

    password = PasswordField('Hasło', validators=[DataRequired()])
    confirm_password = PasswordField('Potwierdź hasło',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('That email is taken. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField('Adres email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Hasło', validators=[DataRequired()])
    remember = BooleanField('Zapamiętaj mnie')
    submit = SubmitField('Login')


class UpdateAccountForm(FlaskForm):
    username = StringField('Nickname',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Adres mail',
                        validators=[DataRequired(), Email()])

    university = SelectField('Universytet',
                           validators=[DataRequired()],
                             choices=[
                                 ("AGH", "AGH"),
                                 ("PK", "PK"),
                                 ("UJ", "UJ"),
                                 ("UR", "UR"),
                                 ("None", "None"),
                             ])

    faculty = SelectField('Wydział',
                           validators=[DataRequired()],
                          choices=[("WGiG","Wydział Górnictwa i Geoinżynierii"),
                            ("WIMiIR","Wydział Inżynierii Metali i Informatyki Przemysłowej"),
                            ("WEAiB","Wydział Elektrotechniki, Automatyki, Informatyki i Inżynierii Biomedycznej"),
                            ("WEIT","Wydział Informatyki, Elektroniki i Telekomunikacji"),
                            ("WIMiR","Wydział Inżynierii Mechanicznej i Robotyki"),
                            ("WGGiOŚ","Wydział Geologii, Geofizyki i Ochrony Środowiska"),
                            ("WGGiIŚ","Wydział Geodezji Górniczej i Inżynierii Środowiska"),
                            ("WIMiC","Wydział Inżynierii Materiałowej i Ceramiki"),
                            ("WO","Wydział Odlewnictwa"),
                            ("WMN","Wydział Metali Nieżelaznych"),
                            ("WWNiG","Wydział Wiertnictwa, Nafty i Gazu"),
                            ("WZ","Wydział Zarządzania"),
                            ("WEiP","Wydział Energetyki i Paliw"),
                            ("WFiIS","Wydział Fizyki i Informatyki Stosowanej"),
                            ("WMS","Wydział Matematyki Stosowanej"),
                            ("WH","Wydział Humanistyczny"),
                            ("None", "None")]
                          )

    social = StringField('Media społecznościowe',
                           validators=[DataRequired(), Length(min=2, max=20)])

    picture = FileField('Dodaj zdjęcie', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Aktualizuj')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('That email is taken. Please choose a different one.')


class ProjectForm(FlaskForm):
    project_name = StringField('Nazwa projektu', validators=[DataRequired()])
    description = TextAreaField('Opis', validators=[DataRequired()])
    coworkers = StringField('Członkowie projektu')
    submit = SubmitField('Stwórz')

class CircleForm(FlaskForm):
    circle_name = StringField('Nazwa koła', validators=[DataRequired()])
    circle_guardian = StringField('Opiekun koła', validators=[DataRequired()])
    description = TextAreaField('Opis', validators=[DataRequired()])
    picture = FileField('Logo koła', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Stwórz')

    def validate_circle_name(self, circle_name):
        circle = Circle.query.filter_by(circle_name=circle_name.data).first()
        if circle:
            raise ValidationError('That circle name is taken. Please choose a different one.')

class UpdateCircleForm(FlaskForm):
    circle_guardian = StringField('Opiekun koła', validators=[DataRequired()])
    description = TextAreaField('Opis', validators=[DataRequired()])
    picture = FileField('Logo koła', validators=[FileAllowed(['jpg', 'png'])])
    circle_members = StringField("Członkowie", validators=[DataRequired()])
    submit = SubmitField('Aktualizuj')

class AddToCircleForm(FlaskForm):
    circle_members = StringField('Nowi członkowie', validators=[DataRequired()])
    submit = SubmitField('Dodaj')

class DeleteFromCircleForm(FlaskForm):
    circle_members = StringField('Usuń członków', validators=[DataRequired()])
    submit = SubmitField('Usuń')

class RequestResetForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Resetowanie hasła')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('There is no account with that email. You must register first.')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Hasło', validators=[DataRequired()])
    confirm_password = PasswordField('Potwierdź hasło',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset hasła')


class CircleSearchForm(FlaskForm):
    search = StringField('Nazwa koła')
    submit = SubmitField('Szukaj')

class UserSearchForm(FlaskForm):
    search = StringField('Nazwa użytkownika')
    submit = SubmitField('Szukaj')

