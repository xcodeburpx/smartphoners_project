import os
import secrets
from PIL import Image
from flask import render_template, url_for, flash, redirect, request, abort
from skillcast import app, db, bcrypt, mail
from skillcast.forms import (RegistrationForm, LoginForm, UpdateAccountForm,
                             ProjectForm, RequestResetForm, ResetPasswordForm, CircleForm, UpdateCircleForm,
                             AddToCircleForm, DeleteFromCircleForm, CircleSearchForm)
from skillcast.models import User, Project, Circle
from flask_login import login_user, current_user, logout_user, login_required
from flask_mail import Message


skills = [
    "c++",
    "python",
    "javascript",
    "soldering",
    "css",
    "php",
    "mig",
    "coaching",
    "5-days facial hair"
]

@app.route("/")
@app.route("/home")
def home():
    if current_user.is_authenticated:
        return redirect(url_for('project_list'))
    else:
        # page = request.args.get('page', 1, type=int)
        # posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
        return render_template('home.html')


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user_skills = request.form.getlist('check')
        user_type = request.form.getlist('radio')[0]
        print(user_type)
        user_type = int(user_type)
        if len(user_skills) == 0:
            user_skills = "None"
        else:
            user_skills = ",".join(user_skills)
        # print(user_skills)
        user = User(name=form.name.data,
                    surname=form.surname.data,
                    username=form.username.data,
                    email=form.email.data,
                    university=form.university.data,
                    faculty=form.faculty.data,
                    social=form.social.data,
                    skills=user_skills,
                    type=user_type,
                    password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Rejestracja', form=form, skills=skills)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Logowanie', form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.university = form.university.data
        current_user.faculty = form.faculty.data
        current_user.social = form.social.data
        user_skills = request.form.getlist('check')
        if len(user_skills) == 0:
            user_skills = "None"
        else:
            user_skills = ",".join(user_skills)
        current_user.skills = user_skills
        if current_user.type == 2:
            circle = Circle.query.filter_by(circle_head=current_user.username).first()
            if circle:
                circle.circle_head = form.username.data
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
        form.university.data = current_user.university
        form.faculty.data = current_user.faculty
        form.social.data = current_user.social
        user_skills = current_user.skills
        if(user_skills != "None"):
            user_skills = user_skills.split(",")
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    projects = Project.query.filter_by(is_circle=False, creator_id=current_user.id).all()
    return render_template('account.html', title='Opcje konta',
                           image_file=image_file, form=form, skills=skills, user_skills=user_skills, projects=projects)

# @app.route("/profile/<string:username>")
# @login_required
# def user_profile(username):
#     user = User.query.filter_by(username=current_user.username).first()
#     return render_template('profile.html', title="{}'s Profile".format(username), user=user)

@app.route("/main_page")
@login_required
def main_page():
    pass


@app.route("/project/new_project", methods=['GET', 'POST'])
@login_required
def new_project():
    form = ProjectForm()
    if form.validate_on_submit():
        project_skills = request.form.getlist('check')
        if len(project_skills) == 0:
            project_skills = "None"
        else:
            project_skills = ",".join(project_skills)
        user_type = current_user.type
        is_circle = False
        creator_id = current_user.id
        if user_type == 2:
            is_circle = True
            circle = Circle.query.filter_by(circle_head=current_user.username).first()
            creator_id = circle.id
        project = Project(
            project_name=form.project_name.data,
            description=form.description.data,
            coworkers=form.coworkers.data,
            creator_id=creator_id,
            is_circle=is_circle,
            skills=project_skills)
        db.session.add(project)
        db.session.commit()
        project = Project.query.filter_by(project_name=form.project_name.data,
                                          description=form.description.data,
                                          coworkers=form.coworkers.data,
                                          creator_id=creator_id,
                                          is_circle=is_circle).first()
        coworkers = form.coworkers.data.split(",")
        final_coworkers = []
        for coworker in coworkers:
            coworker_user = User.query.filter_by(username=coworker).first()
            if coworker_user:
                print(project.id)
                print(coworker_user.projects)
                coworker_user.projects += str(project.id) + ","
                final_coworkers.append(coworker_user.username)
        project.coworkers = ",".join(final_coworkers)
        db.session.commit()
        flash('Your project has been added!', 'success')
        return redirect(url_for('home'))
    return render_template('create_project.html', title='Nowy projekt',
                           form=form, legend='Nowy projekt', skills=skills)


@app.route("/project/<int:project_id>")
def project(project_id):
    project = Project.query.get_or_404(project_id)
    return render_template('project.html', title=project.project_name, project=project)


@app.route("/project/<int:project_id>/update", methods=['GET', 'POST'])
@login_required
def update_project(project_id):
    project = Project.query.get_or_404(project_id)
    if project.author != current_user:
        abort(403)
    form = ProjectForm()
    if form.validate_on_submit():
        project.title = form.title.data
        project.content = form.content.data
        db.session.commit()
        flash('Your project has been updated!', 'success')
        return redirect(url_for('project', project_id=project.id))
    elif request.method == 'GET':
        form.title.data = project.title
        form.content.data = project.content
    return render_template('create_project.html', title='Aktualizuj projekt',
                           form=form, legend='Aktualizuj projekt')


@app.route("/project/<int:project_id>/delete", methods=['POST'])
@login_required
def delete_project(project_id):
    project = Project.query.get_or_404(project_id)
    if project.author != current_user:
        abort(403)
    db.session.delete(project)
    db.session.commit()
    flash('Your project has been removed!', 'success')
    return redirect(url_for('home'))


# @app.route("/user/<string:username>/projects")
# def user_projects(username):
#     page = request.args.get('page', 1, type=int)
#     user = User.query.filter_by(username=username).first_or_404()
#     projects = Project.query.filter_by(author=user)\
#         .order_by(Project.date_posted.desc())\
#         .paginate(page=page, per_page=5)
#     return render_template('user_projects.html', projects=projects, user=user)

@app.route("/user/<string:username>")
def user_profile(username):
    user = User.query.filter_by(username=username).first_or_404()
    project_ids = user.projects.split(',')[:-1]
    project_ids = [int(x) for x in project_ids]
    projects = [Project.query.get(id) for id in project_ids]
    return render_template('user_profile.html', user=user, projects=projects)

@app.route("/new_circle",methods=['GET', 'POST'])
@login_required
def new_circle():
    if current_user.type != 2:
        print('You need to be a circle head to make this operation')
        flash('You need to be a circle head to make this operation', 'warning')
        return redirect(url_for('home'))
    form = CircleForm()
    if form.validate_on_submit():
        circle = Circle.query.filter_by(circle_head=current_user.username).first()
        if circle:
            flash('You cannot create another circle', 'warning')
            return redirect(url_for('home'))
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
        else:
            picture_file = "default.jpg"
        circle_new = Circle(
            circle_name=form.circle_name.data,
            circle_guardian=form.circle_guardian.data,
            circle_head=current_user.username,
            description=form.description.data,
            image_file=picture_file
        )
        db.session.add(circle_new)
        db.session.commit()
        flash('The circle has been created', 'success')
        return redirect(url_for('home'))

    return render_template('new_circle.html', title='Stwórz koło', form=form)

@app.route("/update_circle",methods=['GET', 'POST'])
@login_required
def update_circle():
    if current_user.type != 2:
        print('You need to be a circle head to make this operation')
        flash('You need to be a circle head to make this operation', 'warning')
        return redirect(url_for('home'))
    form = UpdateCircleForm()
    circle = Circle.query.filter_by(circle_head=current_user.username).first()
    if form.validate_on_submit():
        if circle:
            if form.picture.data:
                picture_file = save_picture(form.picture.data)
                circle.image_file = picture_file
            circle.circle_guardian = form.circle_guardian.data
            circle.description = form.description.data
            circle.circle_members = form.circle_members.data
            db.session.commit()
            flash(f'{circle.circle_name} has been updated!', 'success')
            print(f'{circle.circle_name} has been updated!')
            return redirect(url_for('update_circle'))
        else:
            flash(f'{circle.circle_name} update problems!', 'danger')
            print(f'{circle.circle_name} update problems!')
            return redirect(url_for('update_circle'))
    elif request.method == 'GET':
        if circle:
            form.circle_guardian.data = circle.circle_guardian
            form.description.data = circle.description
            form.circle_members.data = circle.circle_members
        else:
            flash(f'{circle.circle_name} update problems!', 'danger')
            print(f'{circle.circle_name} update problems!')
            return redirect(url_for('update_circle'))
    image_file = url_for('static', filename='profile_pics/' + circle.image_file)
    return render_template('update_circle.html', title='Aktualizuj koło', form=form,
                           image_file=image_file, circle_name=circle.circle_name)

@app.route("/circle/<string:circle_name>")
def circle_profile(circle_name):
    circle = Circle.query.filter_by(circle_name=circle_name).first_or_404()
    projects = []
    if (circle.projects != None):
        project_ids = circle.projects.split(',')[:-1]
        project_ids = [int(x) for x in project_ids]
        projects = [Project.query.get(id) for id in project_ids]
    return render_template('circle_profile.html', circle=circle, projects=projects)


@app.route("/add_members", methods=['GET', 'POST'])
@login_required
def add_members():
    if current_user.type != 2:
        print('You need to be a circle head to make this operation')
        flash('You need to be a circle head to make this operation', 'warning')
        return redirect(url_for('home'))
    form = AddToCircleForm()
    circle = Circle.query.filter_by(circle_head=current_user.username).first()
    if form.validate_on_submit():
        if circle:
            new_members = form.circle_members.data.split(",")
            f_members = []
            old_members = circle.circle_members

            if old_members == "None":
                circle.circle_members = ",".join(new_members)
                for new_member in new_members:
                    user = User.query.filter_by(username=new_member).first()
                    s_circles = user.scientific_circles
                    if s_circles == "None":
                        s_circles = circle.circle_name
                        user.scientific_circles = s_circles
                    else:
                        s_circles = s_circles.split(',')
                        if circle.circle_name not in s_circles:
                            s_circles.append(circle.circle_name)
                        user.scientific_circles = ",".join(s_circles)
                    f_members.append(new_member)
            else:
                old_members = old_members.split(",")
                old_members = [x for x in old_members if x != '']

                for new_member in new_members:
                    if new_member not in old_members:
                        user = User.query.filter_by(username=new_member).first()
                        s_circles = user.scientific_circles
                        if s_circles == "None":
                            s_circles = circle.circle_name
                            user.scientific_circles = s_circles
                        else:
                            s_circles = s_circles.split(',')
                            if circle.circle_name not in s_circles:
                                s_circles.append(circle.circle_name)
                            user.scientific_circles = ",".join(s_circles)
                        f_members.append(new_member)
                circle.circle_members += ",".join(f_members)
            db.session.commit()
            flash(f'{circle.circle_name} has gained members!', 'success')
            print(f'{circle.circle_name} has gained members!')
            return redirect(url_for('home'))
        else:
            flash(f'{circle.circle_name} add members problems!', 'danger')
            print(f'{circle.circle_name} add members problems!')
            return redirect(url_for('add_members'))

    return render_template('add_members.html', title='Add Members', form=form)


@app.route("/delete_members", methods=['GET', 'POST'])
@login_required
def delete_members():
    if current_user.type != 2:
        print('You need to be a circle head to make this operation')
        flash('You need to be a circle head to make this operation', 'warning')
        return redirect(url_for('home'))
    form = DeleteFromCircleForm()
    circle = Circle.query.filter_by(circle_head=current_user.username).first()
    if form.validate_on_submit():
        if circle:
            d_members = form.circle_members.data.split(",")
            members = circle.circle_members.split(',')
            f_members = []
            for member in members:
                if member not in d_members:
                    f_members.append(member)
            circle.circle_members = ",".join(f_members)
            for d_member in d_members:
                user = User.query.filter_by(username=d_member).first()
                s_circles = user.scientific_circles.split(',')
                f_circles = []
                for c_name in s_circles:
                    if c_name != circle.circle_name:
                        f_circles.append(c_name)
                if len(f_circles) == 0:
                    user.scientific_circles = "None"
                else:
                    user.scientific_circles = ",".join(f_circles)
            db.session.commit()
            flash(f'{circle.circle_name} successfully deleted members!', 'success')
            print(f'{circle.circle_name} successfully deleted members!')
            return redirect(url_for('home'))
        else:
            flash(f'{circle.circle_name} delete members problems!', 'danger')
            print(f'{circle.circle_name} delete members problems!')
            return redirect(url_for('delete_members'))

    elif request.method == 'GET':
        if circle:
            form.circle_members.data = circle.circle_members
        else:
            flash(f'{circle.circle_name} delete members problems!', 'danger')
            print(f'{circle.circle_name} delete members problems!')
            return redirect(url_for('delete_members'))

    return render_template('delete_members.html', title='Delete Members', form=form)


def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request',
                  sender='noreply@demo.com',
                  recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('reset_token', token=token, _external=True)}

If you did not make this request then simply ignore this email and no changes will be made.
'''
    mail.send(msg)


@app.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


@app.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('reset_token.html', title='Reset Password', form=form)

@app.route("/search_circles", methods=['GET', 'POST'])
@app.route("/search_circles/", methods=['GET', 'POST'])
@app.route("/search_circles/<string:searchname>", methods=['GET', 'POST'])
@login_required
def search_circle(searchname=None):
    if searchname:
        circles = Circle.query.filter(Circle.circle_name.like(f'{searchname}%')).all()
    else:
        circles = Circle.query.all()
    form = CircleSearchForm()
    return render_template('search_circles.html', legend="Circle Search", form=form, circles=circles)

@app.route("/search_users", methods=['GET', 'POST'])
@app.route("/search_users/", methods=['GET', 'POST'])
@app.route("/search_users/<string:searchname>", methods=['GET', 'POST'])
@login_required
def search_user(searchname=None):
    if searchname:
        users = User.query.filter(User.name.like(f'{searchname}%')).all()
        # print(users)
        if len(users) == 0:
            users = User.query.filter(User.username.like(f'{searchname}%')).all()
            # print("Username, ",users)
    else:
        users = User.query.all()
    form = CircleSearchForm()
    return render_template('search_users.html', legend="Circle Search", form=form, users=users)


@app.route("/project_list")
def project_list():
    projects = Project.query.all()
    for project in projects:
        if project.is_circle:
            setattr(project, 'circle', Circle.query.get(project.creator_id).circle_name)
        else:
            setattr(project, 'user', User.query.get(project.creator_id).username)

    return render_template('project_list.html', title='Lista projektów', projects=projects)
