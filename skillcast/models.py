from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from skillcast import db, login_manager, app
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=False, nullable=False)
    surname = db.Column(db.String(20), unique=False, nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    university = db.Column(db.String(50), unique=False, nullable=False, default="None")
    faculty = db.Column(db.String(50), unique=False, nullable=False, default="None")
    social = db.Column(db.String(240), unique=False, nullable=False, default="None")
    skills = db.Column(db.String(2048), unique=False, nullable=False, default="None")
    scientific_circles = db.Column(db.String(2048), default="None")
    type = db.Column(db.Integer, nullable=False, default=0)
    projects = db.Column(db.String(4096), default="")
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.id}','{self.name}', '{self.surname}', '{self.username}'," \
            f" '{self.email}', '{self.university}', '{self.faculty}'," \
            f" '{self.social}', '{self.skills}', '{self.type}', '{self.projects}', " \
            f" '{self.scientific_circles}','{self.image_file}')"


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    project_name = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    description = db.Column(db.Text, nullable=False)
    coworkers = db.Column(db.String(4096), nullable=True)
    creator_id = db.Column(db.Integer, nullable=False)
    skills = db.Column(db.String(2048), unique=False, nullable=False, default="None")
    is_circle = db.Column(db.Boolean, nullable=False, default=False)
    def __repr__(self):
        return f"Project('{self.project_name}', '{self.date_posted.strftime('%Y-%m-%d')}', '{self.id}', '{self.coworkers}', '{self.skills}')"


class Circle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    circle_name = db.Column(db.String(100), nullable=False, unique=True)
    circle_guardian = db.Column(db.String(50), nullable=False)
    circle_head = db.Column(db.String(50), nullable=False, unique=True)
    circle_members = db.Column(db.String(4096), nullable=False, default="None")
    description = db.Column(db.Text, nullable=False, default="Empty description")
    projects = db.Column(db.String(4096))
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')

    def __repr__(self):
        return f"Circle('{self.circle_name}', '{self.circle_guardian}', '{self.circle_head}','{self.circle_members}'," \
            f"'{self.projects}', '{self.image_file}')"

