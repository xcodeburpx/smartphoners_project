Stan projektu:
---

* Panel Rejestracji - gotowe
* Panel Logowania - gotowe
* Strona Głowna - gotowe
* Aktualizacja ustawień profilu studenta - gotowe
* Profil Studenta - WIP
	- Foto, zdjęcie w tle, dane kontaktowe, umiejętności - WIP
	- Przycisk "Obserwuj" - WIP
	- Lista prowadzonych projektów - WIP
* Profil Koła - WIP
	- Foto, zdjęcie w tle, dane kontaktowe do przewodniczącego - WIP
	- Przycisk "Obserwuj" - WIP
	- Lista Członków Koła - WIP
	- Lista Prowadzonych Projektów - WIP
* Zarządzanie Profilem Koła - WIP
	- Akutalizacja ustawień profilu - WIP
	- Dodawanie studentów do Koła - WIP
	- Umieszczanie projektów - WIP
* Projekt "Tablicy" na której wyświetlałyby się projekty obserwowanych osób/kół naukowych - WIP