SKILLCAST PROJECT
=================

PANELE:
---
* Panel Rejestracji
* Panel Logowania

PANEL REJESTRACJI
===
Dane do podania w formularzu:
+ Imię
+ Nazwisko
+ Adres e-mail
+ Uczelnia
+ Wydział
+ Profile społecznościowe (opcjonalnie)

W panelu rejestracji użytkownicy wybierają czy są:
+ Studentami
+ Pracownikami Naukowymi

## Studenci:
Jeśli ktoś jest studentem to ma do wyboru trzy opcje:
+ Przewodniczący Koła - wybór koła, którego jest się przewodniczącym
+ Członek Koła - wybór koła, którego jest się członkiem
+ Brak Koła

## Pracownicy Naukowi:
Jeśli ktoś jest pracownikiem naukowym to ma dwie opcje do wyboru:
+ Opiekun Koła - wybór koła, którego jest się opiekunem
+ Brak Koła

Bez względu na to jaki typ konta zostanie podany, należy dokonać uwierzytelnianbia osoby.

Sposoby uwierzytelniania:
---
## Studenci:
* Przewodniczący Koła:
	- Podanie e-maila domeny uczelnianej.
	- Wysłanie skanu legitymacji.
	- Wskazanie miejsca lub osoby, która jest w stanie potwierdzić pełnioną funkcję przewodniczącego Koła.
* Członek Koła:
	- Podanie e-maila domeny uczelnianej.
	- Przesłanie skanu legitymacji.
	- Wskazanie osoby, która jest w stanie potwierdzić członkostwo w wybranym Kole.
* Studenci bez koła:
	- Podanie e-maila domeny uczelnianej.
	- Przesłanie skanu legitymacji.
## Pracownicy naukowi:
* Opiekun Koła:
	- Podanie e-maila domeny uczelnianej.
	- Przesłanie skanu legitymacji/dokumentu potwierdzającego tożsamość.
	- Wskazanie miejsca lub osoby, która jest w stanie potwierdzić pełnioną funkcję.
* Pracownik Naukowy bez koła:
	- Podanie e-maila domeny uczelnianej.
	- przesłanie skanu legitymacji/dokumentu potwierdzającego tożsamość.
	
PANEL LOGOWANIA
===
Logowanie za pomocą loginu (e-maila) oraz hasła.

