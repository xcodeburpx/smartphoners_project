Skillcast to niesamowita platforma skierowana do studentów stawiających na rozwój w kołach naukowych oraz innych organizacjach studenckich.
Celem platformy jest stworzenie środowiska umożliwiającego prowadzenie projektów interdyscyplinarnych. Daj innym znać o tym w czym jesteś najlepszy,
swoich mocnych stronach i pomysłach. Dołącz do koła naukowego lub zarejestruj własne. Działaj!